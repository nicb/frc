BEGIN {
   FS = "|";
   print("#\n# DO NOT EDIT!\n# Generated automatically by the ./conf/md_to_yaml.awk script\n# from the ../../../matrix.md file\n#")
}
END {
   print("#\n# End of automatically generated conf file\n#")
}
$1 ~ /^[TS]\.[A-Z]/ {
   tag = $1;
   durate = $2;
   agogica = $3;
   freq = $4
   timbro = $5;

   printf("%s:\n  durate: %s\n  agogica: %s\n  frequenze: %s\n  timbro: %s\n", tag, durate, agogica, freq, timbro);
}
