# frozen_string_literal: true

RSpec.describe Frc::FragmentTake do

  before :example do
    @cue_sheet = Frc::CueSheet.new(File.join(FIXTURE_PATH, 'cue_sheet-01.yaml')) 
    @good_take = { args: { 'start' => 1234, 'dur' => 5678, 'good' => true, }, results: { good_should_be: true } }
    @bad_take = { args: { 'start' => 9101112, 'dur' => 131415, }, results: { good_should_be: false } }
    @takes = [ @good_take, @bad_take ]
    @timing_test = { args: { 'start' => 1234, 'dur' => 5678, 'good' => true }, results: { start_time: 1234/Frc::C.sample_rate.to_f, dur_time: 5678/Frc::C.sample_rate.to_f, end_time: (1234+5678)/Frc::C.sample_rate.to_f, } }
    @ref_note = 'a-gis'
  end

  it 'can be created' do
    @takes.each { |f| expect(Frc::FragmentTake.new(f[:args], @ref_note)).not_to be nil }
  end

  it 'sets the good flag properly' do
    @takes.each do
      |f|
      fg = Frc::FragmentTake.new(f[:args], @ref_note)
      expect(fg.good).to eq(f[:results][:good_should_be])
    end
  end

  it 'converts samples in time properly' do
    fg = Frc::FragmentTake.new(@timing_test[:args], @ref_note)
    @timing_test[:results].each { |meth, res| expect(fg.send(meth)).to be_within(1e-6).of(res) }
  end

end
