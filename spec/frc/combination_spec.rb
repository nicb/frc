# frozen_string_literal: true

RSpec.describe Frc::Combination do

  before :example do
    @this = ['R1', 'R4' ]
    @same = ['R1', 'R4' ]
    @diff = ['R1', 'R5' ]
    @tc = Frc::Combination.new(@this[0], @this[1])
    @sc = Frc::Combination.new(@same[0], @same[1])
    @dc = Frc::Combination.new(@diff[0], @diff[1])
  end

  it 'can be created' do
    expect(Frc::Combination.new(@this[0], @this[1])).not_to be nil
  end

  it 'has :==, :=== and :eql? methods that work (positive)' do
    expect(@tc == @sc).to be true
    expect(@tc === @sc).to be true
    expect(@tc.eql?(@sc)).to be true
  end

  it 'has :==, :=== and :eql? methods that work (negative)' do
    expect(@tc == @dc).to be false
    expect(@tc === @dc).to be false
    expect(@tc.eql?(@dc)).to be false
  end

  it 'works with the [].uniq method' do
    duplicate = [@tc, @sc]
    singled_out = duplicate.uniq { |item| [item.first, item.second] }
    expect(duplicate.size).to eq(2)
    expect(singled_out.size).to eq(1)
  end

  it 'has a :is_tuple? that works' do
    a = %w(A B)
    combi = Frc::Combination.new('A', 'B')
    expect(combi.is_tuple?(a)).to be true
  end

end
