# frozen_string_literal: true

RSpec.describe Frc::AudioFragment do

  before :example do
    @fragments =
    [{
       args: { 'name' => 'R1', 'reference_note' => 'a-gis', 'takes' => [ { 'start' => 1266278, 'dur' => 92120 } ] },
       should_be: { 'name' => 'R1', 'reference_note' => 'a-gis', 'takes' => [ { 'start' => 1266278, 'dur' => 92120, 'good' => false } ], good_size: 0 },
     },
     {
       args: { 'name' => 'R2', 'reference_note' => 'f-3', 'takes' => [ { 'start' => 39838006, 'dur' => 60601, 'good' => true } ] },
       should_be: { 'name' => 'R2', 'reference_note' => 'f-3', 'takes' => [ { 'start' => 39838006, 'dur' => 60601, 'good' => true } ], good_size: 1 }
     },
    ]
  end

  it 'can be created' do
    @fragments.each { |f| expect(Frc::AudioFragment.new(f[:args])).not_to be nil }
  end

  it 'has all the properties in place' do
    @fragments.each do
      |f|
      fg = Frc::AudioFragment.new(f[:args])
      expect(fg.name).to eq(f[:should_be]['name'])
      expect(fg.reference_note).to eq(f[:should_be]['reference_note'])
      expect(fg.takes.size).to eq(f[:should_be]['takes'].size)
    end
  end

  it 'has a :good_takes method that works' do
    @fragments.each do
      |f|
      fg = Frc::AudioFragment.new(f[:args])
      expect(fg.takes.size).to eq(f[:should_be]['takes'].size)
      expect(fg.good_takes.size).to eq(f[:should_be][:good_size])
    end
  end

end
