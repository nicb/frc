# frozen_string_literal: true

RSpec.describe Frc::Combinator do

  before :example do
    @conf = File.join(Frc::CONF_PATH, 'combinator.yaml')
    @proper_combinations = construct_combinations(@conf)
  end

  it 'can be created' do
    expect(Frc::Combinator.new(@conf)).not_to be nil
  end

  it "doesn't have fake (equal-equal) combinations" do
    c = Frc::Combinator.new(@conf)
    expect(c.combinations.empty?).not_to be true
    c.combinations.each do
      |c|
      expect(c.first).not_to(eq(c.second),"first_value (#{c.first}) == second_value (#{c.second})")
    end
  end

  it "doesn't have duplicate combinations" do
    c = Frc::Combinator.new(@conf)
    expect(c.combinations.empty?).not_to be true
    cssize = c.combinations.size
    csusize = c.combinations.uniq { |item| [item.first, item.second] }.size
    expect(cssize).to eq(csusize)
  end

  it "doesn't have invalid combinations" do
    c = Frc::Combinator.new(@conf)
    expect(c.combinations.empty?).not_to be true
    c.combinations.each do
      |combi|
      expect(combi.first).not_to be nil
      expect(combi.first).not_to eq ''
      expect(combi.second).not_to be nil
      expect(combi.second).not_to eq ''
    end
  end

  it 'does have all the possible combinations' do
    c = Frc::Combinator.new(@conf)
    @proper_combinations.each do
      |this_combi|
      flag = false
      c.combinations.each do
        |cc|
        if cc.is_tuple?(this_combi)
          flag = true
          break
        end
      end
      expect(flag).to(be(true), "combination not found for tuple #{this_combi}")
    end
  end

end

def construct_combinations(confname)
  res = []
  yaml_conf = YAML.load(File.open(confname, 'r'))
  combis = yaml_conf['combinations']
  combis.each do
    |this_combi|
    as = this_combi['A']
    bs = this_combi['B']
    as.each do
      |this_a|
      res.concat([[this_a] + bs].flatten.permutation(2).to_a.map { |x| x if x.include?(this_a) })
    end
  end
  res.compact
end
