# frozen_string_literal: true

class ForwardableTest
  def method
    23
  end

  def method_with_arguments(a, b, c)
    [a, b, c]
  end
end

class ForwardingTest
  attr_reader :ft
  def initialize
    @ft = ForwardableTest.new
  end
  include Frc::Forward
  forward :ft, :method, :mymethod
  forward :ft, :method
  forward :ft, :method_with_arguments
end

RSpec.describe 'Frc::Forward::forward' do

  before :example do
    @should_be = 23
    @args = [ 1, "one", 3.14591 ]
  end

  it 'works' do
    expect((fdingt = ForwardingTest.new)).not_to be nil
    expect(fdingt.mymethod).to eq(@should_be)
    expect(fdingt.method).to eq(@should_be)
  end

  it 'works with arguments too' do
    expect((fdingt = ForwardingTest.new)).not_to be nil
    expect(fdingt.method_with_arguments(*@args)).to eq(@args)
  end

end
