# frozen_string_literal: true
require 'yaml'

RSpec.describe Frc::AudioFile do

  before :example do
    @files = [File.join(FIXTURE_PATH, 'cue_sheet-01.yaml'), File.join(FIXTURE_PATH, 'cue_sheet-02.yaml')]
    @method_map = { 'file' => 'filename', 'file_index' => 'alias_number', 'sample_rate' => 'sample_rate', 'channels' => 'channels' }
  end

  it 'can be created' do
    @files.each do
      |f|
      properties = YAML.load(File.open(f, 'r'))
      expect(Frc::AudioFile.new(properties)).not_to be nil
    end
  end

  it 'has all the properties in place' do
    @files.each do
      |f|
      properties = YAML.load(File.open(f, 'r'))
      af = Frc::AudioFile.new(properties)
      @method_map.keys.each do
        |m|
        meth = @method_map[m]

        expect(af.respond_to?(meth)).to(be(true), "method: #{meth}")
        expect(af.send(meth)).to(eq(properties[m]), "method: #{meth}: #{af.send(meth)} != #{properties[m]}")
      end
    end
  end

  it 'has a :lookup(tag) method that works' do
    properties = YAML.load(File.open(@files[0], 'r'))
    af = Frc::AudioFile.new(properties)
    keys = af.fragments.keys 
    keys.each { |k| expect(af.lookup(k, 'a-gis').empty?).not_to be true }
    keys.each { |k| expect(af.lookup(k, 'f-e').empty?).not_to be true }
    #
    # test for negatives
    #
    wrong_key = "whatever"
    expect{ af.lookup(wrong_key, 'a-gis') }.to raise_error(Frc::Error)
  end

end
