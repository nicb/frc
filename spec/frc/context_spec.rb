# frozen_string_literal: true

RSpec.describe Frc::Context do

  before :example do
    @fixtures = [File.join(FIXTURE_PATH, 'cue_sheet-01.yaml'), File.join(FIXTURE_PATH, 'cue_sheet-02.yaml')]
    @cue_sheet_1 = Frc::CueSheet.new(@fixtures[0])
    @cue_sheet_2 = Frc::CueSheet.new(@fixtures[1])
    Frc::C.cue_sheet_1 = @cue_sheet_1
    Frc::C.cue_sheet_2 = @cue_sheet_2
    @fwdmethods_1 = [[:audio_file_name, :filename], [:audio_file_index, :alias_number], :sample_rate, :channels]
    @fwdmethods_2 = [[:audio_file_name_2, :filename], [:audio_file_index_2, :alias_number]]
  end

  it 'cannot be created' do
    expect { Frc::Context.new }.to raise_error(NoMethodError)
  end

  it 'exists as a single constant' do
    expect(Frc::C).not_to be nil
  end

  it 'has forwarding methods that work' do
    @fwdmethods_1.each do
      |m|
      cmeth = m.is_a?(Array) ? m[0] : m
      ameth = m.is_a?(Array) ? m[1] : m
      expect(Frc::C.send(cmeth)).to(eq(@cue_sheet_1.audio_file.send(ameth)), "method :#{cmeth} is not properly forwarded!")
    end
    tag = "R1"
    rnote = 'a-gis'
    expect(Frc::C.lookup(tag, rnote)).to eq(@cue_sheet_1.audio_file.lookup(tag, rnote))
    @fwdmethods_2.each do
      |m|
      expect(Frc::C.send(m[0])).to(eq(@cue_sheet_2.audio_file.send(m[1])), "method :#{m[0]} is not properly forwarded (to :#{m[1]})!")
    end
  end

end
