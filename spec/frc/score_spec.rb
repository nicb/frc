# frozen_string_literal: true

RSpec.describe Frc::Score do

  before :example do
    @manager = Frc::Manager.new(File.join(Frc::CONF_PATH, 'manager.yaml')) # needed for everything else to work
    @combi = Frc::Combination.new("R1", "R3")
    @out_path = File.expand_path(File.join(['..'] * 2, 'tmp'), __FILE__)
    @metro = 96
  end

  it 'can be created' do
    expect(Frc::Score.new(@combi, @metro)).not_to be nil 
  end

  it 'can create scores' do
    s = Frc::Score.new(@combi, @metro)
    s.create_score_files(@out_path)
    expect(Dir.glob(File.join(@out_path, '*.sco')).empty?).not_to be true
  end

  it 'can elaborate durations properly' do
    s = Frc::Score.new(@combi, @metro)
    p = 60/@metro * 1
    conf1 = { 'start' => 0, 'dur' => 1000, 'good' => true }
    conf2 = { 'start' => 0, 'dur' => 10000, 'good' => true }
    f1 = Frc::FragmentTake.new(conf1, 'f-e')
    f2 = Frc::FragmentTake.new(conf2, 'f-e')
    res = s.send(:elaborate_cue_sheet_1_first_duration, f1, f2, p)
    expect(res).to eq(f1.dur_time + p)
    #
    # if we invert durations it should be different
    #
    res = s.send(:elaborate_cue_sheet_1_first_duration, f2, f1, p)
    expect(res).to eq(f2.dur_time)
  end

end
