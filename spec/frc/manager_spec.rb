# frozen_string_literal: true

RSpec.describe Frc::Manager do

  before :example do
    @out_path = File.expand_path(File.join(['..'] * 2, 'tmp'), __FILE__)
    @conf = File.expand_path(File.join(['..'] * 3, 'conf', 'manager.yaml'), __FILE__)
  end

  it 'can be created' do
    expect(Frc::Manager.new(@conf)).not_to be nil 
  end

  it 'can create scores' do
    m = Frc::Manager.new(@conf)
    m.to_score_files(@out_path)
    expect(Dir.glob(File.join(@out_path, '*.sco')).empty?).not_to be true
  end

end
