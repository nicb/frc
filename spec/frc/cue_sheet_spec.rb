# frozen_string_literal: true

RSpec.describe Frc::CueSheet do

  before :example do
    @fixtures = [File.join(FIXTURE_PATH, 'cue_sheet-01.yaml'), File.join(FIXTURE_PATH, 'cue_sheet-02.yaml')]
    @forwarded_methods = [[:audio_file_name, :filename], [:audio_file_index, :alias_number], :sample_rate, :channels]
  end

  it 'can be created' do
    @fixtures.each { |f| expect(Frc::CueSheet.new(f)).not_to be nil }
  end

  it 'has forwarded methods that work' do
    cs = []
    @fixtures.each { |f| cs << Frc::CueSheet.new(f) }
    Frc::C.cue_sheet_1 = cs[0]
    Frc::C.cue_sheet_2 = cs[1]
    cs.each do
      |thiscs|
      @forwarded_methods.each do
        |fmm|
        csmeth = fmm.is_a?(Array) ? fmm[0] : fmm
        afmeth = fmm.is_a?(Array) ? fmm[1] : fmm
        expect(thiscs.respond_to?(csmeth)).to be true
        expect(thiscs.send(csmeth)).to eq(thiscs.audio_file.send(afmeth))
      end
    end
  end

end
