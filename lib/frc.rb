# frozen_string_literal: true

module Frc
  class Error < StandardError; end

  %w{
    version
    constants
    forward
    fragment_take
    audio_fragment
    audio_file
    cue_sheet
    context
    combination
    combinator
    manager
    score
  }.each { |f| require_relative File.join('frc', f) }

end
