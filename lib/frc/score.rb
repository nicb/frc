# frozen_string_literal: true

require 'git/describe'

module Frc
  class Score

    attr_reader :combination, :metro

    def initialize(combi, metro)
      @combination = combi
      @metro = metro
    end

    def create_score_files(out_path = DEFAULT_SCO_OUT_PATH)
      create_scores_per_reference_note('a-gis', out_path)
      create_scores_per_reference_note('f-e', out_path)
    end

  private

    def create_scores_per_reference_note(refnote, out_path)
      first_takes = get_takes(self.combination.first, refnote)
      second_takes = get_takes(self.combination.second, refnote)
      raise(Error, "No fragments available for tags '#{self.combination.first}' or '#{self.combination.second}' with reference_notes '#{refnote}'") if first_takes.nil? || second_takes.nil?
      max_size = [first_takes.size, second_takes.size].max
      idx = idx1 = idx2 = 0
      while(idx < max_size)
        frg1 = first_takes[idx1]
        frg2 = second_takes[idx2]
        (s1, totdur, name) = create_cue_sheet_1_score(idx, idx1, idx2, frg1, frg2, metro, refnote)
        res = s1
        res += create_cue_sheet_2_score(totdur)
        save_score(name, res, out_path)
        idx += 1
        idx1 += 1
        idx2 += 1
        idx1 = idx % first_takes.size
        idx2 = idx2 % second_takes.size
      end
    end

    def get_takes(tag, refnote)
      res = C.lookup(tag, refnote).map { |af| af.good_takes }.flatten
      raise(Error, "No fragments available for tag '#{tag}' with reference_notes '#{refnote}'") if res.empty? || res.nil?
      res
    end

    def create_cue_sheet_1_score(idx, i1, i2, frag1, frag2, metro, refnote)
      period = 60.0/metro.to_f * (4.0/8.0) # one eight-note away
      name = create_cue_sheet_1_score_name(idx, i1, i2, self.combination.first, self.combination.second, refnote)
      fskip = frag1.start_time
      fdur = frag1.dur_time
      nextat = elaborate_cue_sheet_1_first_duration(frag1, frag2, period)
      ["; %s\n;\ni1 %12.8f %12.8f 1 %12.8f; %s\ni1 %12.8f %12.8f 1 %12.8f; %s\n" %
        [name, 0, fdur, frag1.start_time, self.combination.first, nextat, frag2.dur_time, frag2.start_time, self.combination.second],
       nextat + frag2.dur_time, name ]
    end

    def create_cue_sheet_1_score_name(idx, i1, i2, tag1, tag2, refnote)
      fname = File.basename(C.audio_file_name, '.yaml')
      version = Git::Describe.git(4)
      "%s-%s-T%d-%s-T%d-%s-%03d-%s" % [fname, tag1, i1, tag2, i2, refnote, idx, version]
    end

    def elaborate_cue_sheet_1_first_duration(frag1, frag2, period)
      frag1.dur_time > frag2.dur_time ? frag1.dur_time + period : frag1.dur_time
    end

    def create_cue_sheet_2_score(tdur)
      amp = C.cue_sheet_2.configuration['amplitude']
      fskip = C.cue_sheet_2.lookup('try 1200', 'a-f').first.takes.first.start_time
      revdur = tdur + 1
      ";\ni2 %12.8f %12.8f 2 %12.8f %+5.1f; modem\ni500 %12.8f %12.8f; reverb\n" % [0, tdur, fskip, amp, 0, revdur]
    end

    def save_score(name, sco, out_path)
      filename = name + '.sco'
      File.open(File.join(out_path, filename), 'w') { |fh| fh.write(sco) }
    end

  end
end
