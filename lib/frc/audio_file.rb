# frozen_string_literal: true

require 'yaml'

module Frc
  class AudioFile

    attr_reader :filename, :alias_number, :sample_rate, :channels, :fragments

    def initialize(conf)
      @filename = conf['file']
      @alias_number = conf['file_index']
      @sample_rate = conf['sample_rate']
      @channels = conf['channels']
      setup(conf['fragments'])
    end

    def lookup(tag, refnote)
      raise Error, "non-existing tag '#{tag}' for reference note '#{refnote}'" unless self.fragments.has_key?(tag)
      self.fragments[tag].map { |af| af if af.reference_note == refnote }.compact
    end

  private

    def setup(fragments)
      @fragments = {}
      fragments.each do
        |frag|
        key = frag['name']
        @fragments.has_key?(key) || @fragments[key] = []
        @fragments[key] << AudioFragment.new(frag)
      end
    end

  end
end
