# frozen_string_literal: true

require 'yaml'

module Frc
  class Combinator

    attr_reader :config_file, :combinations

    def initialize(conf)
      @config_file = conf
      @combinations = []
      setup
    end

  private

    #
    # The configuration file is as follows:
    # - A: [R1, R2]
    #   B: [R3, R4, R5, R6]
    #
    # So we must combine the first A with all the B (and viceversa) and then
    # the second A etc.
    #
    def setup
      res = []
      combis = YAML.load(File.open(self.config_file, 'r'))['combinations']
      combis.each do
        |combi|
        a = combi['A']
        b = combi['B']
        a.each do
          |this_a|
          complete = [ this_a ] + b
          res.concat(complete.permutation(2).to_a.map { |c| c if c.include?(this_a) }.compact)
        end
      end
      res.each do
        |c|
        @combinations << Combination.new(c[0], c[1])
      end
    end

  end
end
