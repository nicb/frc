# frozen_string_literal: true

module Frc
  CONF_PATH = File.expand_path(File.join(['..'] * 3, 'conf'), __FILE__)
  DEFAULT_SCO_OUT_PATH = File.expand_path(File.join(['..'] * 4, 'scores'), __FILE__)
end
