# frozen_string_literal: true

module Frc
  class Context

    #
    # +Frc::Context+ happens to be a singleton which cannot be created
    #
    private_class_method :new

    attr_accessor :cue_sheet_1, :cue_sheet_2

    include Forward

    forward :cue_sheet_1, :audio_file
    forward :cue_sheet_1, :audio_file_name
    forward :cue_sheet_1, :audio_file_index
    forward :cue_sheet_1, :sample_rate
    forward :cue_sheet_1, :channels
    forward :cue_sheet_1, :lookup

    forward :cue_sheet_2, :audio_file, :audio_file_2
    forward :cue_sheet_2, :audio_file_name, :audio_file_name_2
    forward :cue_sheet_2, :audio_file_index, :audio_file_index_2

  end

  C = Context.send(:new) # this is the only existing instance
end
