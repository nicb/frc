# frozen_string_literal: true

require 'yaml'

module Frc
  class CueSheet

    attr_reader :filename, :audio_file, :configuration

    def initialize(file)
      @filename = file
      setup
    end

    include Forward

    forward :audio_file, :filename, :audio_file_name
    forward :audio_file, :alias_number, :audio_file_index
    forward :audio_file, :sample_rate
    forward :audio_file, :channels
    forward :audio_file, :lookup

  private

    def setup
      @configuration = YAML.load(File.open(self.filename, 'r'))
      @audio_file = AudioFile.new(self.configuration)
    end

  end
end
