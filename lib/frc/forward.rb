# frozen_string_literal: true

module Frc
  module Forward

    def self.included(base)
      base.extend ClassMethods
    end

    module ClassMethods

      def forward(obj, objmeth, mymeth = nil)
        mymeth = mymeth || objmeth
        define_method(mymeth) { |*args| self.send(obj).send(objmeth, *args) }
      end

    end
  end
end
