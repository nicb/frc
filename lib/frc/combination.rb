# frozen_string_literal: true

module Frc
  class Combination

    attr_reader :first, :second

    def initialize(first, sec)
      @first = first
      @second = sec
    end

    def ==(other)
      self.first == other.first && self.second == other.second
    end

    alias_method :===, :==
    alias_method :eql?, :==

    #
    # We want to check if a combination ('A', 'B')
    # is existing as an instance of Combination.new('A', 'B')
    #
    def is_tuple?(a)
      self.first == a[0] && self.second == a[1] 
    end

  end
end
