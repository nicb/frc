# frozen_string_literal: true

require 'yaml'

module Frc
  class Manager

    attr_reader :filename, :configuration, :cue_sheets, :combinator, :metro

    def initialize(conf)
      @filename = conf
      @configuration = YAML.load(File.open(self.filename), 'r')
      setup
    end

    def to_score_files(out_path = DEFAULT_SCO_OUT_PATH)
      scores = []
      self.combinator.combinations.each do
        |combi|
        scores << Score.new(combi, self.metro)
      end
      scores.each { |s| s.create_score_files(out_path) }
    end

  private

    def setup
      @metro = self.configuration['metro']
      setup_cue_sheets
      setup_combinator
    end

    def setup_cue_sheets
      @cue_sheets = []
      self.configuration['cue_sheets'].each do
        |cue_file|
        conf_filename = File.join(CONF_PATH, cue_file)
        @cue_sheets << CueSheet.new(conf_filename)
      end
      C.cue_sheet_1 = @cue_sheets.first
      C.cue_sheet_2 = @cue_sheets[1]
    end

    def setup_combinator
      @combinator = Combinator.new(File.join(CONF_PATH, 'combinator.yaml'))
    end
    
  end
end
