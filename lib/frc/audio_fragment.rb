# frozen_string_literal: true

module Frc
  class AudioFragment

    attr_reader :name, :reference_note, :takes

    def initialize(conf)
      @name = conf['name']
      @reference_note = conf['reference_note'] || ''
      setup(conf['takes'])
    end

    def good_takes
      self.takes.map { |t| t if t.good }.compact 
    end

  private

    def setup(takes)
      @takes = []
      takes.each do
        |take|
        @takes << FragmentTake.new(take, self.reference_note)
      end
    end

  end
end
