# frozen_string_literal: true

module Frc
  class FragmentTake

    attr_reader :start, :dur, :good, :reference_note

    def initialize(conf, rn)
      @start = conf['start']
      @dur = conf['dur']
      @good = conf['good'] || false
      @reference_note = rn
    end

    def start_time
      to_time(self.start)
    end

    def dur_time
      to_time(self.dur)
    end

    def end_time
      self.start_time + self.dur_time
    end

  private

    def to_time(samples)
      samples / C.sample_rate.to_f
    end

  end
end
