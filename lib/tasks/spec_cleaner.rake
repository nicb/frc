require 'fileutils'
require 'byebug'

SPEC_PATHS_TO_BE_CLEANED = [ File.expand_path(File.join(['..'] * 3, 'spec', 'tmp'), __FILE__), ]
namespace :spec do

  desc 'clean by-products of spec'
  task 'clean' do
    SPEC_PATHS_TO_BE_CLEANED.each do
      |path|
      cd path do
        Dir.glob('*.sco').each { |f| rm_f f }
      end
    end
  end
end
